#!/usr/bin/env python
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import os
from optparse import OptionParser

usage = '''%prog [options]

This script is intended to link a set of po files for a distinct
language (from pootle.tryton.org) to a Tryton installation, that
can then be used to translate the strings.

Note:
    - The most recent version of a translation is available via
      downloading all po files for a language as a zip file from
      http://pootle.tryton.org. This archive will contain the
      working state of the pootle instance, which you should
      usually use as translation base.
    - The committed version is available from
      https://hg.tryton.org/pootle.

A rough workflow:
    (needs: hgnested, curl, zip, unzip
        i.e. apt-get install mercurial-nested curl zip unzip)

mkdir ~/translation
cd ~/translation

# Get the repos
hg nclone https://hg.tryton.org/trytond
hg clone https://hg.tryton.org/tryton
hg clone https://hg.tryton.org/proteus
hg clone https://hg.tryton.org/sao
hg clone https://hg.tryton.org/pootle
hg clone https://hg.tryton.org/tryton-tools

# Get the actual translation files
curl -o de_DE-tryton.zip http://pootle.tryton.org/export/?path=/de_DE/tryton/
unzip de_DE-tryton.zip

# Use this script to link the pootle po files to the Tryton tree
python link_pootle2tryton.py -s ./de_DE-tryton/de_DE/tryton/ -d . -l de_DE

# Create a Tryton database and translate as usual

# Exporting the Tryton translations
cd ~/translation/trytond
ln -s ../proteus/proteus/ .
cp ../tryton-tools/localize_modules.py .
TRYTOND_CONFIG=./etc/trytond.conf ./localize_modules.py -l de_DE -u admin -d trans2 trytond/

# Pootle needs some non-standard headers set for import
# use convert_tryton2pootle.py to add them
# Note: this script is intended to migrate into a trytond patch to add those
# headers directly on translation export

# Zip the translations
cd ~/translation
zip de_DE-tryton de_DE-tryton/de_DE/tryton/*.po

# Upload the archive to http://pootle.tryton.org/
# Control the result of your work in the Pootle server.

# Done.

# Proposing a codereview
# As changes are easier to follow on a codereview than on pootle, you
# can easily propose a codereview by copying the freshly generated po files
# over the pootle mercurial repos and just use it as the base for the
# codereview.
'''


def main(src_path, dest_path, lang):
    for po_file in os.listdir(src_path):
        repos_name, ext = os.path.splitext(po_file)
        if (po_file.startswith('.') or ext != '.po'):
            continue
        path = os.path.abspath(os.path.join(src_path, po_file))

        if repos_name == 'tryton':
            destination = os.path.join(
                dest_path, 'tryton', 'tryton', 'data', 'locale',
                lang, 'LC_MESSAGES', 'tryton.po')
        elif repos_name == 'sao':
            destination = os.path.join(
                dest_path, 'sao', 'locale', lang + '.po')
        elif repos_name in ['ir', 'res']:
            destination = os.path.join(
                dest_path, 'trytond', 'trytond',
                repos_name, 'locale', lang + '.po')
        else:
            destination = os.path.join(
                dest_path, 'trytond', 'trytond', 'modules',
                repos_name, 'locale', lang + '.po')
        if not os.path.exists(destination):
            os.makedirs(os.path.dirname(destination))
        try:
            os.remove(destination)
        except OSError:
            try:
                os.unlink(destination)
            except OSError:
                pass
            pass
        os.symlink(path, destination)


if __name__ == '__main__':
    parser = OptionParser(usage)
    parser.add_option("-s", "--src", dest="src_path",
                  help="The path to the directory containing the po files")
    parser.add_option("-d", "--dest", dest="dest_path",
                  help="The path to the directory containing the Tryton repos")
    parser.add_option("-l", "--lang", dest="lang",
                  help="The language you want to use, i.e. de_DE")
    opts, args = parser.parse_args()

    if any(o is None for o in [opts.src_path, opts.dest_path, opts.lang]):
        print('\nPlease provide all options.\nTo see the options use %prog -h')
        exit()
    main(opts.src_path, opts.dest_path, opts.lang)
